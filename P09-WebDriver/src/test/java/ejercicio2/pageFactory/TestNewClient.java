package ejercicio2.pageFactory;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

public class TestNewClient {

    WebDriver driver;
    LoginPage poLogin;
    ManagerPage poManager;
    NewCustomerPage poNewCustomer;
    DeleteCustomerPage poDeleteCustomer;


    //Login
    String login = "mngr192645";
    String loginPass = "yqytUga";

    //Customer data
    String customerName = "clienteuno";
    String gender = "m";
    String dob = "1994-01-07";
    String street = "Calle x";
    String city = "Elche";
    String state = "Spain";
    String pin = "123456";
    String phone = "999999999";
    String email = "jst7@alu.ua.es";
    String password = "123456";
    String newId = "";

    // Esperado
    String expTitleHome = "guru99 bank";
    String expTitleManagerLogged = "manger id : " + login;
    String expTitleNewCustomer = "add new customer";
    String expRegisteredSuccessfully = "customer registered successfully!!!";
    String expTitleDeleteCustomer = "delete customer form";
    String expAlert = "do you really want to delete this customer?";
    String expAlert2 = "customer deleted successfully";
    String expAlertEmailRepeat = "email address already exist !!";


    @BeforeEach
    void init() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        poLogin = PageFactory.initElements(driver, LoginPage.class);
    }

    @AfterEach
    void close() {
        driver.close();
    }

    @Test
    public void testTestNewClientOk() {
        //LOGIN
        //Pagina inicial para login
        assertTrue(poLogin.getPageTitle().toLowerCase().contains(expTitleHome));

        //MANAGER
        //Llamamos al login del PageObject de Login y devolvemos un Manager
        poManager = poLogin.login(login, loginPass);
        //Comprobamos que estamos en la PageObject de Manager
        assertTrue(poManager.getHomePageDashboardUserName().toLowerCase().contains(expTitleManagerLogged));

        //NEWCUSTOMER
        //Pulsamos el click en nuevo cliente y cambiamos de PageObject
        poNewCustomer = poManager.newCustomer();
        //Comprobamos que estamos en la pagina de NewCustomer
        assertTrue(poNewCustomer.getPageTitle().toLowerCase().contains(expTitleNewCustomer));
        //Llamamos a la inserción del nuevo cliente
        poNewCustomer.newCustomer(customerName, gender, dob, street, city, state, pin, phone, email, password);
        //Comprobamos que se realice correctamente con el titulo de correcto
        assertTrue(poNewCustomer.getSuccessTitle().toLowerCase().contains(expRegisteredSuccessfully));
        //Almacenamos el id generado
        newId = poNewCustomer.getNewId();
        System.out.println(newId);

        //Vamos a la pagina de eliminar
        poDeleteCustomer = poManager.deleteCustomer();
        assertTrue(poDeleteCustomer.getPageTitle().toLowerCase().contains(expTitleDeleteCustomer));
        poDeleteCustomer.deleteCustomer(newId);

        //Comprobamos los alert
        assertTrue(expAlert.toLowerCase().contains(poDeleteCustomer.getAlert()));
        assertTrue(expAlert2.toLowerCase().contains(poDeleteCustomer.getAlert()));


    }

    @Test
    public void testTestNewClientDuplicate() {
        //LOGIN
        //Pagina inicial para login
        assertTrue(poLogin.getPageTitle().toLowerCase().contains(expTitleHome));

        //MANAGER
        //Llamamos al login del PageObject de Login y devolvemos un Manager
        poManager = poLogin.login(login, loginPass);
        //Comprobamos que estamos en la PageObject de Manager
        assertTrue(poManager.getHomePageDashboardUserName().toLowerCase().contains(expTitleManagerLogged));

        //NEWCUSTOMER
        //Pulsamos el click en nuevo cliente y cambiamos de PageObject
        poNewCustomer = poManager.newCustomer();
        //Comprobamos que estamos en la pagina de NewCustomer
        assertTrue(poNewCustomer.getPageTitle().toLowerCase().contains(expTitleNewCustomer));
        //Llamamos a la inserción del nuevo cliente
        poNewCustomer.newCustomer(customerName, gender, dob, street, city, state, pin, phone, email, password);
        //Comprobamos que se realice correctamente con el titulo de correcto
        assertTrue(poNewCustomer.getSuccessTitle().toLowerCase().contains(expRegisteredSuccessfully));
        //Almacenamos el id generado
        newId = poNewCustomer.getNewId();
        System.out.println(newId);

        //Comprobamos el cliente creado que ya esta insertado
        poNewCustomer = poManager.newCustomer();
        assertTrue(poNewCustomer.getPageTitle().toLowerCase().contains(expTitleNewCustomer));
        poNewCustomer.newCustomer(customerName, gender, dob, street, city, state, pin, phone, email, password);
        // Comprobamos que falla por repetir email
        assertEquals(expAlertEmailRepeat, poNewCustomer.repeatCustomerMessage());

        //Vamos a la pagina de eliminar
        poDeleteCustomer = poManager.deleteCustomer();
        assertTrue(poDeleteCustomer.getPageTitle().toLowerCase().contains(expTitleDeleteCustomer));
        poDeleteCustomer.deleteCustomer(newId);

        //Comprobamos los alert
        assertTrue(expAlert.toLowerCase().contains(poDeleteCustomer.getAlert()));
        assertTrue(expAlert2.toLowerCase().contains(poDeleteCustomer.getAlert()));
    }
}
