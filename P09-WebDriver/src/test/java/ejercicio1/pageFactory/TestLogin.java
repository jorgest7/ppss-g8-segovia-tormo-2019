package ejercicio1.pageFactory;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

public class TestLogin {

    WebDriver driver;
    LoginPage poLogin;
    ManagerPage poManager;

    @BeforeEach
    void init() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        poLogin = PageFactory.initElements(driver, LoginPage.class);
    }

    @AfterEach
    void close() {
       driver.close();
    }

    @Test
    public void test_Login_Correct() {
        String tittle = poLogin.getPageTitle();
        assertTrue(tittle.toLowerCase().contains("guru99 bank"));

        poLogin.login("mngr192645","yqytUga");

        poManager = PageFactory.initElements(driver, ManagerPage.class);
        assertTrue(poManager.getHomePageDashboardUserName().toLowerCase().contains("manger id : mngr192645"));
    }

    @Test
    public void test_Login_Incorrect() {

        String esperado = "User or Password is not valid";

        String tittle = poLogin.getPageTitle();
        assertTrue(tittle.toLowerCase().contains("guru99 bank"));

        poLogin.login("mal","mal");
        assertEquals(esperado, poLogin.closeAlert());
    }

}
