package ejercicio2.pageFactory;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

public class DeleteCustomerPage {
    WebDriver driver;

    @FindBy(xpath="//table/tbody/tr/td/table/tbody/tr[1]/td/p")
    WebElement pTitle;
    @FindBy(name="cusid")
    WebElement inputId;
    @FindBy(name="AccSubmit")
    WebElement submit;


    public DeleteCustomerPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getPageTitle() {
        return this.pTitle.getText();
    }

    public void deleteCustomer(String id) {
        inputId.sendKeys(id);
        submit.click();
    }

    public String getAlert() {
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.accept();

        return alertText.toLowerCase();
    }
}
