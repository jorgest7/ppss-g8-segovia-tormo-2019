package ejercicio2.pageFactory;

import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

public class NewCustomerPage {

    WebDriver driver;
    @FindBy(name="name")
    WebElement inputCustomerName;
    @FindBy(xpath="//table/tbody/tr/td/table/tbody/tr[5]/td[2]/input[1]")
    WebElement inputGeneroM;
    @FindBy(xpath="//table/tbody/tr/td/table/tbody/tr[5]/td[2]/input[2]")
    WebElement inputGeneroF;
    @FindBy(name="dob")
    WebElement inputDob;
    @FindBy(name="addr")
    WebElement inputAddr;
    @FindBy(name="city")
    WebElement inputCity;
    @FindBy(name="state")
    WebElement inputState;
    @FindBy(name="pinno")
    WebElement inputPin;
    @FindBy(name="telephoneno")
    WebElement inputTelephoneno;
    @FindBy(name="emailid")
    WebElement inputEmailid;
    @FindBy(name="password")
    WebElement inputPassword;
    @FindBy(xpath="//table/tbody/tr/td/table/tbody/tr[14]/td[2]/input[1]")
    WebElement submit;
    @FindBy(xpath="//table/tbody/tr/td/table/tbody/tr[1]/td/p")
    WebElement pTitle;
    @FindBy(xpath="//table/tbody/tr/td/table/tbody/tr[1]/td/p")
    WebElement registerTitle;
    @FindBy(xpath="//table/tbody/tr/td/table/tbody/tr[4]/td[2]")
    WebElement lastInsertId;

    public NewCustomerPage(WebDriver driver) {
        this.driver = driver;
    }

    public void newCustomer(String newLogin, String genero, String fecha,
                            String direccion, String ciudad, String estado,
                            String pin, String movil, String email, String password) {

        JavascriptExecutor js = (JavascriptExecutor) driver;

        inputCustomerName.sendKeys(newLogin);
        if (genero.equals('m')) {
            inputGeneroM.click();
        } else {
            inputGeneroF.click();
        }
        inputDob.click();
        inputDob.sendKeys(fecha);
        inputAddr.sendKeys(direccion);
        inputCity.sendKeys(ciudad);
        inputState.sendKeys(estado);
        inputPin.sendKeys(pin);
        inputTelephoneno.sendKeys(movil);
        inputEmailid.sendKeys(email);
        inputPassword.sendKeys(password);

        js.executeScript("arguments[0].scrollIntoView();", submit);
        submit.click();
    }

    public String getPageTitle() {
        return this.pTitle.getText();
    }

    public String getSuccessTitle() {
        return this.registerTitle.getText();
    }

    public String getNewId() {
        return this.lastInsertId.getText();
    }

    public String repeatCustomerMessage() {
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.accept();

        return alertText.toLowerCase();
    }
}
