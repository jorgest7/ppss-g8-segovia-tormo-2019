package ppss;

import org.easymock.EasyMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ppss.excepciones.IsbnInvalidoException;
import ppss.excepciones.JDBCException;
import ppss.excepciones.ReservaException;
import ppss.excepciones.SocioInvalidoException;

import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.anyString;
import static org.junit.jupiter.api.Assertions.*;

class ReservaStubTest {

    Reserva mockReservaParcial;
    IOperacionBO mockIOperacion;
    FactoriaBOs mockFactoria;

    String login, password, idSocio;

    @BeforeEach
    void init() {
        mockReservaParcial = EasyMock.partialMockBuilder(Reserva.class).addMockedMethods("getFactoriaBOs", "compruebaPermisos").createNiceMock();
        mockIOperacion = EasyMock.createNiceMock(IOperacionBO.class);
        mockFactoria = EasyMock.createNiceMock(FactoriaBOs.class);
    }

    @Test
    void realizaReservaC1() {
        login = "xxxx";
        password = "xxxx";
        idSocio = "Pepe";
        String[] isbns = {"22222"};

        String resultadoEsperado = "ERROR de permisos; ";

        EasyMock.expect(mockReservaParcial.compruebaPermisos(anyString(), anyString(), anyObject())).andStubReturn(false);
        EasyMock.replay(mockReservaParcial);

        ReservaException real = assertThrows(ReservaException.class, () -> mockReservaParcial.realizaReserva(login, password, idSocio, isbns));
        assertEquals(resultadoEsperado, real.getMessage());
    }

    @Test
    void realizaReservaC2() throws Exception {
        login = "ppss";
        password = "ppss";
        idSocio = "Pepe";
        String[] isbns = {"22222", "33333"};

        EasyMock.expect(mockReservaParcial.compruebaPermisos(anyString(), anyString(), anyObject())).andStubReturn(true);
        EasyMock.expect(mockReservaParcial.getFactoriaBOs()).andStubReturn(mockFactoria);
        EasyMock.expect(mockFactoria.getOperacionBO()).andStubReturn(mockIOperacion);

        for (String isbn : isbns) {
            mockIOperacion.operacionReserva(anyString(), anyString());
        }
        EasyMock.replay(mockReservaParcial, mockFactoria, mockIOperacion);

        mockReservaParcial.realizaReserva(login, password, idSocio, isbns);
        assertTrue(true);
    }

    @Test
    void realizaReservaC3() throws Exception{
        login = "ppss";
        password = "ppss";
        idSocio = "Pepe";
        String[] isbns = {"11111"};

        String resultadoEsperado = "ISBN invalido:11111; ";

        EasyMock.expect(mockReservaParcial.compruebaPermisos(anyString(), anyString(), anyObject())).andStubReturn(true);
        EasyMock.expect(mockReservaParcial.getFactoriaBOs()).andStubReturn(mockFactoria);
        EasyMock.expect(mockFactoria.getOperacionBO()).andStubReturn(mockIOperacion);

        mockIOperacion.operacionReserva(anyString(), anyString());
        EasyMock.expectLastCall().andStubThrow(new IsbnInvalidoException());

        EasyMock.replay(mockReservaParcial, mockFactoria, mockIOperacion);

        ReservaException real = assertThrows(ReservaException.class, () -> mockReservaParcial.realizaReserva(login, password, idSocio, isbns));
        assertEquals(resultadoEsperado, real.getMessage());

    }

    @Test
    void realizaReservaC4() throws Exception{
        login = "ppss";
        password = "ppss";
        idSocio = "Luis";
        String[] isbns = {"22222"};

        String resultadoEsperado = "SOCIO invalido; ";

        EasyMock.expect(mockReservaParcial.compruebaPermisos(anyString(), anyString(), anyObject())).andStubReturn(true);
        EasyMock.expect(mockReservaParcial.getFactoriaBOs()).andStubReturn(mockFactoria);
        EasyMock.expect(mockFactoria.getOperacionBO()).andStubReturn(mockIOperacion);

        mockIOperacion.operacionReserva(anyString(), anyString());
        EasyMock.expectLastCall().andStubThrow(new SocioInvalidoException());

        EasyMock.replay(mockReservaParcial, mockFactoria, mockIOperacion);

        ReservaException real = assertThrows(ReservaException.class, () -> mockReservaParcial.realizaReserva(login, password, idSocio, isbns));
        assertEquals(resultadoEsperado, real.getMessage());

    }

    @Test
    void realizaReservaC5() throws Exception{
        login = "ppss";
        password = "ppss";
        idSocio = "Pepe";
        String[] isbns = {"22222"};

        String resultadoEsperado = "CONEXION invalida; ";

        EasyMock.expect(mockReservaParcial.compruebaPermisos(anyString(), anyString(), anyObject())).andStubReturn(true);
        EasyMock.expect(mockReservaParcial.getFactoriaBOs()).andStubReturn(mockFactoria);
        EasyMock.expect(mockFactoria.getOperacionBO()).andStubReturn(mockIOperacion);

        mockIOperacion.operacionReserva(anyString(), anyString());
        EasyMock.expectLastCall().andStubThrow(new JDBCException());

        EasyMock.replay(mockReservaParcial, mockFactoria, mockIOperacion);

        ReservaException real = assertThrows(ReservaException.class, () -> mockReservaParcial.realizaReserva(login, password, idSocio, isbns));
        assertEquals(resultadoEsperado, real.getMessage());

    }
}