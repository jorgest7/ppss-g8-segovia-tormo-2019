package ppss;

import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ppss.excepciones.IsbnInvalidoException;
import ppss.excepciones.JDBCException;
import ppss.excepciones.ReservaException;
import ppss.excepciones.SocioInvalidoException;

import static org.junit.jupiter.api.Assertions.*;

class ReservaTest {

    Reserva mockReservaParcial;
    IOperacionBO mockIOperacion;
    FactoriaBOs mockFactoria;

    String login, password, idSocio;

    @BeforeEach
    void init() {
        mockReservaParcial = EasyMock.partialMockBuilder(Reserva.class).addMockedMethods("getFactoriaBOs", "compruebaPermisos").createMock();
        mockIOperacion = EasyMock.createStrictMock(IOperacionBO.class);//Dentro del for, mas de una llamada
        mockFactoria = EasyMock.createMock(FactoriaBOs.class);
    }

    @Test
    void realizaReservaC1() {
        login = "xxxx";
        password = "xxxx";
        idSocio = "Pepe";
        String[] isbns = {"22222"};// en el momento de la declaracion

        String resultadoEsperado = "ERROR de permisos; ";

        EasyMock.expect(mockReservaParcial.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)).andReturn(Boolean.FALSE);
        EasyMock.replay(mockReservaParcial);

        ReservaException real = assertThrows(ReservaException.class, () -> mockReservaParcial.realizaReserva(login, password, idSocio, isbns));
        EasyMock.verify(mockReservaParcial);
        assertEquals(resultadoEsperado, real.getMessage());
    }

    @Test
    void realizaReservaC2() throws Exception {
        login = "ppss";
        password = "ppss";
        idSocio = "Pepe";
        String[] isbns = {"22222","33333"};

        EasyMock.expect(mockReservaParcial.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)).andReturn(Boolean.TRUE);

        EasyMock.expect(mockReservaParcial.getFactoriaBOs()).andReturn(mockFactoria);
        EasyMock.expect(mockFactoria.getOperacionBO()).andReturn(mockIOperacion);

        for (String isbn : isbns) { //Por esta iteracion se obliga a ser strict
            mockIOperacion.operacionReserva(idSocio, isbn);
        }

        EasyMock.replay(mockFactoria, mockIOperacion, mockReservaParcial);

        mockReservaParcial.realizaReserva(login, password, idSocio, isbns);
        EasyMock.verify(mockFactoria, mockIOperacion, mockReservaParcial);
        assertTrue(true);

    }

    @Test
    void realizaReservaC3() throws Exception {
        login = "ppss";
        password = "ppss";
        idSocio = "Pepe";
        String[] isbns = {"11111"};

        String resultadoEsperado = "ISBN invalido:11111; ";

        EasyMock.expect(mockReservaParcial.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)).andReturn(true);
        EasyMock.expect(mockReservaParcial.getFactoriaBOs()).andReturn(mockFactoria);
        EasyMock.expect(mockFactoria.getOperacionBO()).andReturn(mockIOperacion);

        mockIOperacion.operacionReserva(idSocio, isbns[0]);
        EasyMock.expectLastCall().andThrow(new IsbnInvalidoException());

        EasyMock.replay(mockReservaParcial, mockFactoria, mockIOperacion);

        ReservaException real = assertThrows(ReservaException.class, () -> mockReservaParcial.realizaReserva(login, password, idSocio, isbns));

        assertEquals(resultadoEsperado, real.getMessage());
        EasyMock.verify(mockReservaParcial, mockFactoria, mockIOperacion);
    }

    @Test
    void realizaReservaC4() throws IsbnInvalidoException, SocioInvalidoException, JDBCException {
        login = "ppss";
        password = "ppss";
        idSocio = "Pepe";
        Boolean bd = true;
        String[] isbns = {"11111"};

        String resultadoEsperado = "SOCIO invalido; ";

        EasyMock.expect(mockReservaParcial.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)).andReturn(true);
        EasyMock.expect(mockReservaParcial.getFactoriaBOs()).andReturn(mockFactoria);
        EasyMock.expect(mockFactoria.getOperacionBO()).andReturn(mockIOperacion);

        mockIOperacion.operacionReserva(idSocio, isbns[0]);
        EasyMock.expectLastCall().andThrow(new SocioInvalidoException());

        EasyMock.replay(mockReservaParcial, mockFactoria, mockIOperacion);

        ReservaException real = assertThrows(ReservaException.class, () -> mockReservaParcial.realizaReserva(login, password, idSocio, isbns));

        EasyMock.verify(mockReservaParcial, mockFactoria, mockIOperacion);
        assertEquals(resultadoEsperado, real.getMessage());
    }

    @Test
    void realizaReservaC5() throws IsbnInvalidoException, SocioInvalidoException, JDBCException {
        login = "ppss";
        password = "ppss";
        idSocio = "Pepe";
        Boolean bd = false;
        String[] isbns = {"11111"};

        String resultadoEsperado = "CONEXION invalida; ";

        EasyMock.expect(mockReservaParcial.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO)).andReturn(true);
        EasyMock.expect(mockReservaParcial.getFactoriaBOs()).andReturn(mockFactoria);
        EasyMock.expect(mockFactoria.getOperacionBO()).andReturn(mockIOperacion);

        mockIOperacion.operacionReserva(idSocio, isbns[0]);
        EasyMock.expectLastCall().andThrow(new JDBCException());

        EasyMock.replay(mockReservaParcial, mockFactoria, mockIOperacion);

        ReservaException real = assertThrows(ReservaException.class, () -> mockReservaParcial.realizaReserva(login, password, idSocio, isbns));

        EasyMock.verify(mockReservaParcial, mockFactoria, mockIOperacion);
        assertEquals(resultadoEsperado, real.getMessage());
    }
}