package ppss;

import org.easymock.EasyMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PremioStrictTest {

    Premio mockParcial;
    ClienteWebService clienteMock;

    @BeforeEach
    void init() {
        mockParcial = EasyMock.partialMockBuilder(Premio.class).addMockedMethod("generaNumero").createMock();
        clienteMock = EasyMock.createStrictMock(ClienteWebService.class);
    }

    float aleatorio;
    String obtenerPremio;

    String resultadoEsperado;

    @Test
    void compruebaPremioC1() throws ClienteWebServiceException {
        aleatorio = 0.07f;
        obtenerPremio = "entrada final Champions";

        resultadoEsperado = "Premiado con entrada final Champions";

        EasyMock.expect(mockParcial.generaNumero()).andReturn(aleatorio);
        EasyMock.replay(mockParcial);

        mockParcial.cliente = clienteMock;

        EasyMock.expect(clienteMock.obtenerPremio()).andReturn(obtenerPremio);
        EasyMock.replay(clienteMock);

        String resultadoReal = mockParcial.compruebaPremio();

        assertEquals(resultadoEsperado, resultadoReal);
        EasyMock.verify(mockParcial, clienteMock);

    }

    @Test
    void compruebaPremioC2() throws ClienteWebServiceException {
        aleatorio = 0.03f;

        resultadoEsperado = "No se ha podido obtener el premio";

        EasyMock.expect(mockParcial.generaNumero()).andReturn(aleatorio);
        EasyMock.replay(mockParcial);

        mockParcial.cliente = clienteMock;

        EasyMock.expect(clienteMock.obtenerPremio()).andThrow(new ClienteWebServiceException());
        EasyMock.replay(clienteMock);

        String resultadoReal = mockParcial.compruebaPremio();

        assertEquals(resultadoEsperado, resultadoReal);
        EasyMock.verify(mockParcial, clienteMock);
    }
}