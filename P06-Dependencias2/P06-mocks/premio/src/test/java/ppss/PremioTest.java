package ppss;

import org.easymock.EasyMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PremioTest {

    ClienteWebService mockClienteWS;
    Premio parcialMock;

    @BeforeEach
    void init() {
        //Con el partialMock modificamos el numero aleatorio y podemos sobreescribir el objeto cliente sin necesidad de refactorizar por que es public
        parcialMock = EasyMock.partialMockBuilder(Premio.class).addMockedMethods("generaNumero").createMock();
        //Como solo se llama una vez podemos usar un mock normal
        mockClienteWS = EasyMock.createMock(ClienteWebService.class);
    }

    float aleatorio;
    String obtenerPremio;

    String resultadoEsperado;
    String resultadoReal;
    @Test
    void compruebaPremioC1() throws ClienteWebServiceException {
        aleatorio = 0.07f;
        obtenerPremio = "entrada final Champions";

        resultadoEsperado = "Premiado con entrada final Champions";

        EasyMock.expect(parcialMock.generaNumero()).andReturn(aleatorio);
        EasyMock.replay(parcialMock);

        parcialMock.cliente = mockClienteWS; //El cliente que queremos es el mockCliente, al ser publico lo metemos asi

        EasyMock.expect(mockClienteWS.obtenerPremio()).andReturn(obtenerPremio);
        EasyMock.replay(mockClienteWS);

        //Llamada al SUT
        resultadoReal = parcialMock.compruebaPremio();

        EasyMock.verify(parcialMock, mockClienteWS);
        assertEquals(resultadoEsperado, resultadoReal);
    }

    @Test
    void compruebaPremioC2() throws ClienteWebServiceException {
        aleatorio = 0.03f;
        resultadoEsperado = "No se ha podido obtener el premio";

        String resultadoReal;

        EasyMock.expect(parcialMock.generaNumero()).andReturn(aleatorio);
        EasyMock.replay(parcialMock);

        parcialMock.cliente = mockClienteWS;

        EasyMock.expect(mockClienteWS.obtenerPremio()).andThrow(new ClienteWebServiceException());//Excepcion
        EasyMock.replay(mockClienteWS);

        resultadoReal = parcialMock.compruebaPremio();

        EasyMock.verify(parcialMock, mockClienteWS);
        assertEquals(resultadoEsperado, resultadoReal);
    }
}