package ppss;

public class ClienteWebServiceException extends Exception {

    public ClienteWebServiceException(){}

    public ClienteWebServiceException(String msg) {
        super(msg);
    }
}
