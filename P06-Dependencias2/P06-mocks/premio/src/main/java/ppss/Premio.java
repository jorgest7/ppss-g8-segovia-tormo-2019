package ppss;

import java.util.Random;

public class Premio {
    private static final float PROBABILIDAD_PREMIO = 0.1f;
    public Random generador = new Random(System.currentTimeMillis());
    public ClienteWebService cliente = new ClienteWebService();

    public String compruebaPremio() {

        if (generaNumero() < PROBABILIDAD_PREMIO) {
            try {
                String premio = cliente.obtenerPremio(); //OLD refactorizado //No hace falta refactorizar porque al ser public se puede sobreescribir

                //NEW refactorizado
                //ClienteWebService clienteRefac = getCliente();
                //String premio = clienteRefac.obtenerPremio();

                return "Premiado con " + premio;
            } catch (ClienteWebServiceException e) {
                return "No se ha podido obtener el premio";
            }
        } else {
            return "Sin premio";
        }

    }

    //refactorizado
    //public ClienteWebService getCliente() {
    //    return cliente;
    //}

    // Genera numero aleatorio entre 0 y 1
    public float generaNumero() {
        return generador.nextFloat();
    }
}
