package ppss;

import org.easymock.EasyMock;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GestorLlamadasTest {

    Calendario mockCalendario;
    GestorLlamadas  mockParcial;

    @BeforeEach
    void init() {
        mockCalendario = EasyMock.createMock(Calendario.class);
        mockParcial = EasyMock.partialMockBuilder(GestorLlamadas.class).addMockedMethod("getCalendario").createMock();

    }

    @Test
    void calculaConsumoC1() {
        int minutos = 22;
        int hora = 10;
        double resultadoEsperado = 457.6;
        double resultadoReal;

        EasyMock.expect(mockParcial.getCalendario()).andReturn(mockCalendario);
        EasyMock.replay(mockParcial);

        EasyMock.expect(mockCalendario.getHoraActual()).andReturn(hora);
        EasyMock.replay(mockCalendario);

        resultadoReal = mockParcial.calculaConsumo(minutos);

        EasyMock.verify(mockParcial, mockCalendario);
        assertEquals(resultadoEsperado, resultadoReal);
    }

    @Test
    void calcularCosumoC2() {
        int minutos = 13;
        int hora = 21;
        double resultadoEsperado = 136.5;
        double resultadoReal;

        EasyMock.expect(mockParcial.getCalendario()).andReturn(mockCalendario);
        EasyMock.replay(mockParcial);

        EasyMock.expect(mockCalendario.getHoraActual()).andReturn(hora);
        EasyMock.replay(mockCalendario);

        resultadoReal = mockParcial.calculaConsumo(minutos);

        EasyMock.verify(mockParcial, mockCalendario);
        assertEquals(resultadoEsperado, resultadoReal);
    }
}