package ppss;

import org.junit.jupiter.api.Test;

import javax.xml.crypto.Data;

import static org.junit.jupiter.api.Assertions.*;

class DataArrayTest {

    DataArray col;
    int[] coleccion;
    int tam;
    int resultadoEsperadoTam, resultadoRealTam;
    int[] resultadoEsperadoCol, resultadoRealCol;

    //Añadir Elemento coleccion vacia
    @Test
    void testAddC1() {
        coleccion = new int []{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        tam = 0;
        col = new DataArray(coleccion, tam);
        int elem = 5;

        resultadoEsperadoTam = 1;
        resultadoEsperadoCol = new int []{5, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        col.add(elem);
        resultadoRealTam = col.size();
        resultadoRealCol = col.getColeccion();

        assertEquals(resultadoEsperadoTam, resultadoRealTam);
        assertArrayEquals(resultadoEsperadoCol, resultadoRealCol);

    }

    //Añadir Elemento coleccion de mas de un elemento
    @Test
    void testAddC2() {
        coleccion = new int []{2, 3, 5, 0, 0, 0, 0, 0, 0, 0};
        tam = 3;
        col = new DataArray(coleccion, tam);
        int elem = 8;

        resultadoEsperadoTam = 4;
        resultadoEsperadoCol = new int []{2, 3, 5, 8, 0, 0, 0, 0, 0, 0};
        col.add(elem);
        resultadoRealTam = col.size();
        resultadoRealCol = col.getColeccion();

        assertEquals(resultadoEsperadoTam, resultadoRealTam);
        assertArrayEquals(resultadoEsperadoCol, resultadoRealCol);
    }

    //Añadir Elemento a coleccion llena
    @Test
    void testAddC3() {
        coleccion = new int []{2, 3, 4, 7, 3, 1, 9, 6, 9, 7};
        tam = 10;
        col = new DataArray(coleccion, tam);
        int elem = 5;

        resultadoEsperadoTam = 10;
        resultadoEsperadoCol = new int []{2, 3, 4, 7, 3, 1, 9, 6, 9, 7};
        col.add(elem);
        resultadoRealTam = col.size();
        resultadoRealCol = col.getColeccion();

        assertEquals(resultadoEsperadoTam, resultadoRealTam);
        assertArrayEquals(resultadoEsperadoCol, resultadoRealCol);
    }

    @Test
    void testDeleteC1() {
        coleccion = new int []{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        tam = 0;
        col = new DataArray(coleccion, tam);
        int elem = 7;

        resultadoEsperadoTam = 0;
        resultadoEsperadoCol = new int []{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        col.delete(elem);
        resultadoRealTam = col.size();
        resultadoRealCol = col.getColeccion();

        assertEquals(resultadoEsperadoTam, resultadoRealTam);
        assertArrayEquals(resultadoEsperadoCol, resultadoRealCol);        }

    @Test
    void testDeleteC2() {
        coleccion = new int []{1, 2, 3, 4, 5, 6, 0, 0, 0, 0};
        tam = 6;
        col = new DataArray(coleccion, tam);
        int elem = 3;

        resultadoEsperadoTam = 5;
        resultadoEsperadoCol = new int []{1, 2, 4, 5, 6, 0, 0, 0, 0, 0};
        col.delete(elem);
        resultadoRealTam = col.size();
        resultadoRealCol = col.getColeccion();

        assertEquals(resultadoEsperadoTam, resultadoRealTam);
        assertArrayEquals(resultadoEsperadoCol, resultadoRealCol);        }
}