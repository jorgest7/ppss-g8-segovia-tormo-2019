package ppss;


public class DataArray {
    private int[] coleccion;
    private int numElem;
    
    //Constructor
    public DataArray() {
        coleccion = new int[10];
        numElem=0;        
    }
    
    //Constructor
    public DataArray(int[] datos, int contador) {
        coleccion = datos;
        numElem=contador;        
    }
    
    public int size() {
        return numElem;
    }
    
    //getter
    public int[] getColeccion() {
        return coleccion;
    }
    
    //método para añadir un entero a la colección
    public void add(int elem) {
    if (numElem < (coleccion.length)) {
            coleccion[numElem]= elem;
            numElem++;
            System.out.println("added "+elem +" ahora hay "+numElem+ " elementos");
        } else {
            System.out.println(elem +" ya no cabe. Ya has añadido "+numElem+" elementos");
        } 
    }


    //Implementado metodo delete 5-B
    //método para borrar un entero a la colección
    public int[] delete(int elem) {
        int[] result = new int []{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        int cont = 0;
        for (int i =0; i < coleccion.length; i++) {
            if (coleccion[i] != elem) {
                result[cont] = coleccion[i];
                cont ++;
            } else {
                System.out.println("Reducimos un elemento el tamaño de " +
                                numElem + " a " + (numElem - 1) +
                        " de la coleccion quitamos el valor: " + elem);
                numElem--;
            }
        }
        coleccion = result;
        return result;
    }
    
}
