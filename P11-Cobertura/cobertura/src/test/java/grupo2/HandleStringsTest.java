package grupo2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HandleStringsTest {

    HandleStrings hs;

    @BeforeEach
    void init() {
        hs = new HandleStrings();
    }

    @Test
    void extractMiddle() {
        String in = "jorge:segovia:tormo";
        String esperado = "segovia";
        String real = hs.extractMiddle(in);
        assertEquals(esperado, real);
    }
}