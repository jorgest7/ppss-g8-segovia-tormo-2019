package grupo1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MultipathExampleTest {

    MultipathExample ex;
    int a, b, c;

    @BeforeEach
    void init () {
        ex = new MultipathExample();
    }

    @Test
    void multiPath_C1() {
        a = 1;
        b = 1;
        c = 1;
        int esperado = 1;
        int real = ex.multiPath(a, b, c);

        assertEquals(esperado, real);
    }

    @Test
    void multiPath_C2() {
        a = 6;
        b = 6;
        c = 1;

        int esperado = 13;
        int real = ex.multiPath(a, b, c);
        assertEquals(esperado, real);
    }

}