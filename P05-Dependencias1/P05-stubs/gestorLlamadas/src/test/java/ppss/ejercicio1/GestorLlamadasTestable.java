package ppss.ejercicio1;

public class GestorLlamadasTestable extends GestorLlamadas {
    private int horaActual;

    @Override
    public int getHoraActual() {
        return horaActual;
    }

    public void setHoraActual(int horaActual) {
        this.horaActual = horaActual;
    }
}

