package ppss.ejercicio1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GestorLlamadasTest {

    GestorLlamadasTestable gestorTestable;
    int minutos;
    int hora;
    double resultadoEsperado;
    double resultadoReal;

    @BeforeEach
    void init() {
        gestorTestable = new GestorLlamadasTestable();
    }

    @Test
    void calculaConsumoC1() {
        minutos = 10;
        hora = 15;
        gestorTestable.setHoraActual(hora);

        resultadoEsperado = 208;

        resultadoReal = gestorTestable.calculaConsumo(minutos);

        assertEquals(resultadoEsperado, resultadoReal);
    }

    @Test
    void calculaConsumoC2() {
        minutos = 10;
        hora = 22;
        gestorTestable.setHoraActual(hora);

        resultadoEsperado = 105;

        resultadoReal = gestorTestable.calculaConsumo(minutos);

        assertEquals(resultadoEsperado, resultadoReal);
    }
}