package ppss.ejercicio2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GestorLlamadasTest {


    GestorLlamadasTestable gestorLlamadas;
    CalendarioStub calStub;
    int minutos;
    int hora;
    double resultadoEsperado;
    double resultadoReal;

    @BeforeEach
    void init() {
        gestorLlamadas = new GestorLlamadasTestable();
        calStub = new CalendarioStub();
    }

    @Test
    void calculaConsumoC1() {
        minutos = 10;
        hora = 15;

        resultadoEsperado = 208;
        calStub.setHoraActual(hora);
        gestorLlamadas.setCalendario(calStub);
        resultadoReal = gestorLlamadas.calculaConsumo(minutos);

        assertEquals(resultadoEsperado, resultadoReal);

    }

    @Test
    void calculaConsumoC2() {
        minutos = 10;
        hora = 22;

        resultadoEsperado = 105;
        calStub.setHoraActual(hora);
        gestorLlamadas.setCalendario(calStub);
        resultadoReal = gestorLlamadas.calculaConsumo(minutos);

        assertEquals(resultadoEsperado, resultadoReal);

    }
}