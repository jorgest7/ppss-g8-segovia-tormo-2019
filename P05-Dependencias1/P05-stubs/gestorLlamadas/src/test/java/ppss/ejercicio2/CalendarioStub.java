package ppss.ejercicio2;

public class CalendarioStub extends Calendario {
    private int horaActual;
    
    @Override
    public int getHoraActual() {
        return horaActual;
    }

    public void setHoraActual(int horaActual) {
        this.horaActual = horaActual;
    }
}
