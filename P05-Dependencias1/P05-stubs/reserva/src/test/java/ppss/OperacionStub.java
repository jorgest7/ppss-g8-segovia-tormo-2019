package ppss;

import ppss.excepciones.IsbnInvalidoException;
import ppss.excepciones.JDBCException;
import ppss.excepciones.SocioInvalidoException;

import java.util.Arrays;

public class OperacionStub implements IOperacionBO {

    //Reemplaza los datos de la bd
    private boolean conexionBd;
    private String socio;
    private String[] isbn;

    public OperacionStub(boolean bd) { //Mejor con un setter y el valor de bd a true de clase
        conexionBd = bd;
        socio = "Luis";
        isbn = new String[]{"11111", "22222"};
    }

    @Override
    public void operacionReserva(String socio, String isbn) throws IsbnInvalidoException, JDBCException, SocioInvalidoException {

        if(!this.socio.equals(socio)) {
            throw new SocioInvalidoException();
        }
        if(!conexionBd) {
            throw new JDBCException();
        }
        if(!Arrays.asList(this.isbn).contains(isbn)) {
            throw new IsbnInvalidoException();
        }
    }
}
