package ppss;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ppss.excepciones.ReservaException;

import static org.junit.jupiter.api.Assertions.*;

class ReservaTest {

    String login;
    String password;
    String idSocio;
    boolean accesoBd;
    String[] isbns;

    String resultadoEsperado;

    ReservaTestable sut;

    @BeforeEach
    void init() {
        sut = new ReservaTestable();
    }

    @Test
    void realizaReservaC1() {
        login = "xxxx";
        password = "xxxx";
        idSocio = "Luis";
        accesoBd = false; //Indica que no se accede, no se usa por la traza de la conexion
        isbns = new String[]{"11111"};

        resultadoEsperado = "ERROR de permisos; ";

        sut.setPermiso(true);

        IOperacionBO operacionStub = new OperacionStub(accesoBd);
        OperacionFactory.setOperacion(operacionStub);

        ReservaException resultadoReal = assertThrows(ReservaException.class, () -> sut.realizaReserva(login, password, idSocio, isbns));
        assertEquals(resultadoEsperado, resultadoReal.getMessage());
    }

    @Test
    void realizaReservaC2() {
        login = "ppss";
        password = "ppss";
        idSocio = "Luis";
        accesoBd = true;
        isbns = new String[]{"11111", "22222"};

        resultadoEsperado = "";

        sut.setPermiso(true);

        IOperacionBO operacionStub = new OperacionStub(accesoBd);
        OperacionFactory.setOperacion(operacionStub);

        try {
            sut.realizaReserva(login, password, idSocio, isbns);
            assertTrue (true);
        } catch (Exception e) {
            fail("Excepcion no esperada");
        }
    }

    @Test
    void realizaReservaC3() {
        login = "ppss";
        password = "ppss";
        idSocio = "Luis";
        accesoBd = true;
        isbns = new String[]{"33333"};

        resultadoEsperado = "ISBN invalido:33333; ";

        sut.setPermiso(true);

        IOperacionBO operacionStub = new OperacionStub(accesoBd);
        OperacionFactory.setOperacion(operacionStub);

        ReservaException resultadoReal = assertThrows(ReservaException.class, () -> sut.realizaReserva(login, password, idSocio, isbns));
        assertEquals(resultadoEsperado, resultadoReal.getMessage());

    }

    @Test
    void realizaReservaC4() {
        login = "ppss";
        password = "ppss";
        idSocio = "Pepe";
        accesoBd = true;
        isbns = new String[]{"11111"};

        resultadoEsperado = "SOCIO invalido; ";

        sut.setPermiso(true);

        IOperacionBO operacionStub = new OperacionStub(accesoBd);
        OperacionFactory.setOperacion(operacionStub);

        ReservaException resultadoReal = assertThrows(ReservaException.class, () -> sut.realizaReserva(login, password, idSocio, isbns));
        assertEquals(resultadoEsperado, resultadoReal.getMessage());

    }

    @Test
    void realizaReservaC5() {
        login = "ppss";
        password = "ppss";
        idSocio = "Luis";
        accesoBd = false;
        isbns = new String[]{"11111"};

        resultadoEsperado = "CONEXION invalida; ";

        sut.setPermiso(true);

        IOperacionBO operacionStub = new OperacionStub(accesoBd);
        OperacionFactory.setOperacion(operacionStub);

        ReservaException resultadoReal = assertThrows(ReservaException.class, () -> sut.realizaReserva(login, password, idSocio, isbns));
        assertEquals(resultadoEsperado, resultadoReal.getMessage());

    }


}