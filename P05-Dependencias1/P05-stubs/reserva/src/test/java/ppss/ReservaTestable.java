package ppss;

import com.sun.org.apache.xpath.internal.operations.Bool;

public class ReservaTestable extends Reserva {

    private Boolean permiso;

    @Override
    public boolean compruebaPermisos(String login, String password, Usuario tipoUsu) {
        return permiso;
    }

    public void setPermiso(boolean per) {
        permiso = per;
    }
}
