package ppss;

public class OperacionFactory {
    private static IOperacionBO obj;

    public static IOperacionBO create() {
        if(obj == null) {
            return new Operacion();
        } else {
            return obj;
        }
    }

    static void setOperacion(IOperacionBO io) {
        obj = io;
    }
}
