package ppss.matriculacion.dao;

import org.apache.log4j.BasicConfigurator;
import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.util.fileloader.DataFileLoader;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;
import org.junit.BeforeClass;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import ppss.matriculacion.to.AlumnoTO;

import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.*;

@Tag("Integracion-fase1")
class AlumnoDAOIT {

    public static final String TABLE_ALUMNOS = "alumnos";
    private IDatabaseTester databaseTester;

    @BeforeClass
    public static void only_once() {
        BasicConfigurator.configure();

    }

    @BeforeEach
    public void setUp() throws Exception {
        databaseTester = new JdbcDatabaseTester("com.mysql.jdbc.Driver",
                "jdbc:mysql://localhost:3306/matriculacion?useSSL=false", "root", "ppss");

        //DataFileLoader loader = new FlatXmlDataFileLoader();
        IDataSet dataSet = new FlatXmlDataFileLoader().load("/tabla2.xml");
        databaseTester.setDataSet(dataSet);
        databaseTester.onSetup();
    }

    @Test
    public void testA1() throws Exception {

        //Datos entrada
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, 1985);
        cal.set(Calendar.MONTH, 1);
        cal.set(Calendar.DATE, 22);

        AlumnoTO alumno = new AlumnoTO();
        alumno.setNif("33333333C");
        alumno.setNombre("Elena Aguirre Juarez");
        alumno.setFechaNacimiento(cal.getTime());
        alumno.setDireccion("");
        alumno.setEmail("");

        //esperado
        DataFileLoader loader = new FlatXmlDataFileLoader();
        IDataSet expectedDataSet = loader.load("/tabla3.xml");
        ITable expectedTable = expectedDataSet.getTable(TABLE_ALUMNOS);

        //Agregar alumno
        new FactoriaDAO().getAlumnoDAO().addAlumno(alumno);

        //Recuperar el dataset
        IDatabaseConnection connection = databaseTester.getConnection();
        IDataSet databaseDataSet = connection.createDataSet();
        ITable actualTable = databaseDataSet.getTable(TABLE_ALUMNOS);

        Assertion.assertEquals(expectedTable, actualTable);
    }

    @Test
    public void testA2() throws Exception {

        //Datos entrada
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, 1982);
        cal.set(Calendar.MONTH, 1);
        cal.set(Calendar.DATE, 22);

        AlumnoTO alumno = new AlumnoTO();
        alumno.setNif("11111111A");
        alumno.setNombre("Alfonso Ramirezz Ruiz");
        alumno.setFechaNacimiento(cal.getTime());
        alumno.setDireccion("");
        alumno.setEmail("");

        //Agregar alumno
        DAOException real = assertThrows(DAOException.class, ()-> new FactoriaDAO().getAlumnoDAO().addAlumno(alumno));
        //assertEquals("Error al conectar con BD", real.getMessage());
        assertTrue(true);
    }

    @Test
    public void testA3() throws Exception {

        //Datos entrada
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, 1982);
        cal.set(Calendar.MONTH, 1);
        cal.set(Calendar.DATE, 22);

        AlumnoTO alumno = new AlumnoTO();
        alumno.setNif("44444444D");
        alumno.setNombre(null);
        alumno.setFechaNacimiento(cal.getTime());
        alumno.setDireccion("");
        alumno.setEmail("");

        //Agregar alumno
        DAOException real = assertThrows(DAOException.class, ()-> new FactoriaDAO().getAlumnoDAO().addAlumno(alumno));
        //assertEquals("Error al conectar con BD", real.getMessage());
        assertTrue(true);
    }

    @Test
    public void testA4() throws Exception {

        //Datos entrada
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, 1982);
        cal.set(Calendar.MONTH, 1);
        cal.set(Calendar.DATE, 22);

        AlumnoTO alumno = null;

        //Agregar alumno
        DAOException real = assertThrows(DAOException.class, ()-> new FactoriaDAO().getAlumnoDAO().addAlumno(alumno));
        //assertEquals("Error alumno no puede ser nulo", real.getMessage());
        assertTrue(true);
    }

    @Test
    public void testA5() throws Exception {

        //Datos entrada
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.YEAR, 1982);
        cal.set(Calendar.MONTH, 1);
        cal.set(Calendar.DATE, 22);

        AlumnoTO alumno = new AlumnoTO();
        alumno.setNif(null);
        alumno.setNombre("Pedro Garcia Lopez");
        alumno.setFechaNacimiento(cal.getTime());
        alumno.setDireccion("");
        alumno.setEmail("");

        //Agregar alumno
        DAOException real = assertThrows(DAOException.class, ()-> new FactoriaDAO().getAlumnoDAO().addAlumno(alumno));
        //assertEquals("Error al conectar con BD", real.getMessage());
        assertTrue(true);

    }

    @Test
    public void testB1() throws Exception {

        //Datos entrada
        String nif = "11111111A";

        //esperado
        DataFileLoader loader = new FlatXmlDataFileLoader();
        IDataSet expectedDataSet = loader.load("/tabla4.xml");
        ITable expectedTable = expectedDataSet.getTable(TABLE_ALUMNOS);

        //Eliminar al alumno
        new FactoriaDAO().getAlumnoDAO().delAlumno(nif);

        //Recuperar el dataset
        IDatabaseConnection connection = databaseTester.getConnection();
        IDataSet databaseDataSet = connection.createDataSet();
        ITable actualTable = databaseDataSet.getTable(TABLE_ALUMNOS);

        Assertion.assertEquals(expectedTable, actualTable);
    }

    @Test
    public void testB2() throws Exception {

        //Datos entrada
        String nif = "33333333C";

        //Eliminar al alumno
        DAOException real = assertThrows(DAOException.class, ()-> new FactoriaDAO().getAlumnoDAO().delAlumno(nif));
        //assertEquals("No se ha borrado ningun alumno", real.getMessage());
        assertTrue(true);
    }




}