package ppss;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class LlanosTest {

    ArrayList<Integer> lecturas;
    Llanos llanos;

    Tramo resultadoEsperadoTramo;
    Tramo resultadoRealTramo;


    @BeforeEach
    void init() {
        llanos = new Llanos();
        lecturas = new ArrayList<>();
    }

    @Test
    @Tag("llanosA")
    void C1A_buscarTramoLlanoMasLargo() {
        lecturas.addAll(Arrays.asList(3));
        resultadoEsperadoTramo = new Tramo(0, 0);

        resultadoRealTramo = llanos.buscarTramoLlanoMasLargo(lecturas);

        //Objeto
        assertEquals(resultadoEsperadoTramo, resultadoRealTramo);

        //Caracteristicas
        assertEquals(resultadoEsperadoTramo.longitud, resultadoRealTramo.longitud);
        assertEquals(resultadoEsperadoTramo.origen, resultadoRealTramo.origen);
    }

    @Test
    @Tag("llanosA")
    void C2A_buscarTramoLlanoMasLargo() {
        lecturas.addAll(Arrays.asList(100, 100, 100, 100));
        resultadoEsperadoTramo = new Tramo(0, 3);

        resultadoRealTramo = llanos.buscarTramoLlanoMasLargo(lecturas);

        //Objeto
        assertEquals(resultadoEsperadoTramo, resultadoRealTramo);

        //Caracteristicas
        assertEquals(resultadoEsperadoTramo.longitud, resultadoRealTramo.longitud);
        assertEquals(resultadoEsperadoTramo.origen, resultadoRealTramo.origen);
    }

    @Test
    @Tag("llanosA")
    void C3A_buscarTramoLlanoMasLargo() {
        lecturas.addAll(Arrays.asList(120, 140, 180, 180, 180));
        resultadoEsperadoTramo = new Tramo(2, 2);

        resultadoRealTramo = llanos.buscarTramoLlanoMasLargo(lecturas);

        //Objeto
        assertEquals(resultadoEsperadoTramo, resultadoRealTramo);

        //Caracteristicas
        assertEquals(resultadoEsperadoTramo.longitud, resultadoRealTramo.longitud);
        assertEquals(resultadoEsperadoTramo.origen, resultadoRealTramo.origen);
    }


    @Test
    @Tag("llanosB")
    void C1B_buscarTramoLlanoMasLargo() {
        lecturas.addAll(Arrays.asList(-1));
        resultadoEsperadoTramo = new Tramo(0, 0);

        resultadoRealTramo = llanos.buscarTramoLlanoMasLargo(lecturas);

        //Objeto
        assertEquals(resultadoEsperadoTramo, resultadoRealTramo);

        //Caracteristicas
        assertEquals(resultadoEsperadoTramo.longitud, resultadoRealTramo.longitud);
        assertEquals(resultadoEsperadoTramo.origen, resultadoRealTramo.origen);
    }


    @Test
    @Tag("llanosB")
    void C2B_buscarTramoLlanoMasLargo() {
        lecturas.addAll(Arrays.asList(-1, -1, -1, -1));
        resultadoEsperadoTramo = new Tramo(0, 3);

        resultadoRealTramo = llanos.buscarTramoLlanoMasLargo(lecturas);

        //Objeto
        assertEquals(resultadoEsperadoTramo, resultadoRealTramo);

        //Caracteristicas
        assertEquals(resultadoEsperadoTramo.longitud, resultadoRealTramo.longitud);
        assertEquals(resultadoEsperadoTramo.origen, resultadoRealTramo.origen);
    }


    @Test
    @Tag("llanosB")
    void C3B_buscarTramoLlanoMasLargo() {
        lecturas.addAll(Arrays.asList(120, 140, -10, -10, -10));
        resultadoEsperadoTramo = new Tramo(2, 2);

        resultadoRealTramo = llanos.buscarTramoLlanoMasLargo(lecturas);

        //Objeto
        assertEquals(resultadoEsperadoTramo, resultadoRealTramo);

        //Caracteristicas
        assertEquals(resultadoEsperadoTramo.longitud, resultadoRealTramo.longitud);
        assertEquals(resultadoEsperadoTramo.origen, resultadoRealTramo.origen);
    }
}