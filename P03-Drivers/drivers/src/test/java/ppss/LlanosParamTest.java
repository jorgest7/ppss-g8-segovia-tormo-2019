package ppss;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class LlanosParamTest {

    Llanos llanos;
    Tramo resultadoRealTramo;


    @BeforeEach
    void init() {
        llanos = new Llanos();
    }

    @ParameterizedTest
    @Tag("Parametrizados")
    @MethodSource("casosDePrueba")
    void buscarTramoLlanoMasLargo( ArrayList<Integer> lecturas, Tramo resultadoEsperadoTramo) {
        resultadoRealTramo = llanos.buscarTramoLlanoMasLargo(lecturas);

        assertEquals(resultadoEsperadoTramo, resultadoRealTramo, () -> generateMessageFail(lecturas, resultadoEsperadoTramo));
    }

    private static Stream<Arguments> casosDePrueba() {

        return Stream.of(
                Arguments.of(new ArrayList<Integer>(Arrays.asList(3)), new Tramo(0, 0)),
                Arguments.of(new ArrayList<Integer>(Arrays.asList(100, 100, 100, 100)), new Tramo(0, 3)),
                Arguments.of(new ArrayList<Integer>(Arrays.asList(120, 140, 180, 180, 180)), new Tramo(2, 2)),
                Arguments.of(new ArrayList<Integer>(Arrays.asList(-1)), new Tramo(0, 0)),
                Arguments.of(new ArrayList<Integer>(Arrays.asList(-1, -1, -1, -1)), new Tramo(0, 3)),
                Arguments.of(new ArrayList<Integer>(Arrays.asList(120, 140, -10, -10, -10)), new Tramo(2, 2))
        );
    }

    private String generateMessageFail(ArrayList<Integer> lecturas, Tramo esperado) {
        return "Error de las lecturas: " + lecturas + "Con el tramo: long: " + esperado.longitud + " origen: " + esperado.origen;
    }
}