package ppss;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MatriculaParamTest {

    Matricula mat;
    float resultadoReal;

    @BeforeEach
    public void init() {
        mat = new Matricula();
    }

    @ParameterizedTest(name = "Con la edad {1} y familia numerosa {2} y repetidor {3} debería que pagar {0}")
    @Tag("Parametrizados")
    @MethodSource("casosDePrueba")
    void calculaTasaMatricula(float resultadoEsperado, int edad, boolean familiaNumerosa, boolean repetidor) {
        resultadoReal = mat.calculaTasaMatricula(edad, familiaNumerosa, repetidor);

        //puedes pasar o menasaje o lambda
        assertEquals(resultadoEsperado, resultadoReal, () -> generateMessageFail(edad, familiaNumerosa, repetidor));
    }

    private static Stream<Arguments> casosDePrueba() {
        return Stream.of(
                Arguments.of(2000, 19, false, true),
                Arguments.of(250, 68, false, true),
                Arguments.of(250, 19, true, true),
                Arguments.of(500, 19, false, false),
                Arguments.of(400, 61, false, false)
        );
    }

    private String generateMessageFail(int edad, boolean familiaNumerosa, boolean repetidor) {
        return "El usuario con: " + edad + familiaNumerosa + repetidor + " no encaja lo esperado con su resultado real";
    }
}