package ppss;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

class DataArrayTest {

    DataArray dataArray;

    @BeforeEach
    void init() {
        dataArray = new DataArray();
    }

    @Test
    void deleteTest_C1() throws DataException {

        int[] datosEsperado = {1,3,7,0,0,0,0,0,0,0};
        int tamEsperado = 3;

        dataArray = new DataArray(new int[]{1,3,5,7,0,0,0,0,0,0}, 4);
        int[] deleteReal = dataArray.delete(5);

        assertAll("Errores C1", //Comprobar lo que devuelve el delete, es necesario
                () -> assertArrayEquals(datosEsperado, dataArray.getColeccion()),
                () -> assertEquals(tamEsperado, dataArray.size()),
                () -> assertArrayEquals(datosEsperado, deleteReal)
        );
    }

    @Test
    void deleteTest_C2() throws DataException {

        int[] datosEsperado = {1,3,5,7,0,0,0,0,0,0};
        int tamEsperado = 4;

        dataArray = new DataArray(new int[]{1,3,3,5,7,0,0,0,0,0}, 5);
        int[] deleteReal = dataArray.delete(3);

        assertAll("Errores C2",
                () -> assertArrayEquals(datosEsperado, dataArray.getColeccion()),
                () -> assertEquals(tamEsperado, dataArray.size()),
                () -> assertArrayEquals(datosEsperado, deleteReal)
        );
    }

    @Test
    void deleteTest_C3() throws DataException {

        int[] datosEsperado = {1,2,3,5,6,7,8,9,10,0};
        int tamEsperado = 9;

        dataArray = new DataArray(new int[]{1,2,3,4,5,6,7,8,9,10}, 10);
        int[] deleteReal = dataArray.delete(4);

        assertAll("Errores C3",
                () -> assertArrayEquals(datosEsperado, dataArray.getColeccion()),
                () -> assertEquals(tamEsperado, dataArray.size()),
                () -> assertArrayEquals(datosEsperado, deleteReal)
        );
    }

    @Test
    void deleteTest_C4() {
        String esperado = "No hay elementos en la colección";

        dataArray = new DataArray(new int[]{0,0,0,0,0,0,0,0,0,0}, 0);
        DataException dat = assertThrows(DataException.class, ()-> dataArray.delete(8));
        assertEquals(esperado, dat.getMessage());
    }

    @Test
    void deleteTest_C5() {
        String esperado = "El valor a borrar debe ser > cero";

        dataArray = new DataArray(new int[]{1,3,5,7,0,0,0,0,0,0}, 4);
        DataException dat = assertThrows(DataException.class, () -> dataArray.delete(-5));
        assertEquals(esperado, dat.getMessage());
    }

    @Test
    void deleteTest_C6() {//DUDA
        String esperado = "No hay elementos en la colección. Y el valor a borrar debe ser > 0";

        dataArray = new DataArray(new int[]{0,0,0,0,0,0,0,0,0,0}, 0);
        DataException dat = assertThrows(DataException.class, () -> dataArray.delete(0));
        assertEquals(esperado, dat.getMessage());
    }

    @Test
    void deleteTest_C7() {
        String esperado = "Elemento no encontrado";

        dataArray = new DataArray(new int[]{1,3,5,7,0,0,0,0,0}, 4);
        DataException dat = assertThrows(DataException.class, () -> dataArray.delete(8));
        assertEquals(esperado, dat.getMessage());
    }
}