1) la diferencia entre mvn compiler:testCompile y mvn test-compile:
goal mvn compiler:testCompile solo realiza la compilacion de los test, con el proyecto limpio no funciona
fase mvn test-compile realiza todo hasta la compilacion de los test (compila código), con el proyecto limpio funciona

    <dependencies>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>5.3.1</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <!-- JUnit 5 requires Surefire version 2.22.0 or higher -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.22.1</version>
            </plugin>
        </plugins>
    </build>

    Sin añadir el surefire no se ejecutan los test porque no es compatible con la versión